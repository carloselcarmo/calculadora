package br.com.calculadora.controllers;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController
{
    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora)
    {
        realizarValidacoesBasicas(calculadora);
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora)
    {
        realizarValidacoesBasicas(calculadora);
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora)
    {
        realizarValidacoesBasicas(calculadora);
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora)
    {
        realizarValidacoesBasicas(calculadora);
        validarNumerosOrdemDecrescenteDivisao(calculadora);
        return calculadoraService.dividir(calculadora);
    }

    private void realizarValidacoesBasicas(Calculadora calculadora)
    {
        validarQuantidadeNumerosRecebidos(calculadora);
        validarNumerosNaturais(calculadora);
    }

    private void validarQuantidadeNumerosRecebidos(Calculadora calculadora)
    {
        if(calculadora.getNumeros().size() <= 1)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário informar pelo menos 2 números");
        }
    }

    private void validarNumerosNaturais(Calculadora calculadora)
    {
        for(Integer numero : calculadora.getNumeros())
        {
            if(numero < 0)
            {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só são aceitos número naturais na calculadora");
            }
        }
    }

    private void validarNumerosOrdemDecrescenteDivisao(Calculadora calculadora)
    {
        int ultimoNumero = calculadora.getNumeros().get(0);

        for(int i = 1; i < calculadora.getNumeros().size(); i++)
        {
            if(ultimoNumero < calculadora.getNumeros().get(i))
            {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só são aceitos números em ordem decrescente para a divisão");
            }
            ultimoNumero = calculadora.getNumeros().get(i);
        }
    }
}
