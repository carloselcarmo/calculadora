package br.com.calculadora.DTOs;

public class RespostaDTO
{
    private Integer Resultado;

    public Integer getResultado() {
        return Resultado;
    }

    public void setResultado(Integer resultado) {
        Resultado = resultado;
    }

    public RespostaDTO()
    {
    }

    public RespostaDTO(Integer resultado)
    {
        Resultado = resultado;
    }
}
