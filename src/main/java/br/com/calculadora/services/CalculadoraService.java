package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService
{
    public RespostaDTO somar(Calculadora calculadora)
    {
        Integer resultado = 0;
        for(Integer numero : calculadora.getNumeros())
        {
            resultado += numero;
        }
        return  (new RespostaDTO (resultado));
    }

    public RespostaDTO subtrair(Calculadora calculadora)
    {
        Integer resultado = 0;

        if(calculadora.getNumeros().size() > 0)
            resultado = calculadora.getNumeros().get(0);

        for(int i = 1; i < calculadora.getNumeros().size(); i++)
        {
            resultado -= calculadora.getNumeros().get(i);
        }

        return  (new RespostaDTO (resultado));
    }

    public RespostaDTO multiplicar(Calculadora calculadora)
    {
        Integer resultado = 0;

        if(calculadora.getNumeros().size() > 0)
            resultado = calculadora.getNumeros().get(0);

        for(int i = 1; i < calculadora.getNumeros().size(); i++)
        {
            resultado *= calculadora.getNumeros().get(i);
        }

        return  (new RespostaDTO (resultado));
    }

    public RespostaDTO dividir(Calculadora calculadora)
    {
        Integer resultado = 0;

        if(calculadora.getNumeros().size() > 0)
            resultado = calculadora.getNumeros().get(0);

        for(int i = 1; i < calculadora.getNumeros().size(); i++)
        {
            resultado /= calculadora.getNumeros().get(i);
        }

        return  (new RespostaDTO (resultado));
    }
}
